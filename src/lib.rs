#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(dead_code)]

#[macro_use]
extern crate libc;
extern crate shared_library;
extern crate dvk_khr_surface;
#[macro_use]
extern crate dvk;

use self::libc::{c_void, c_char, uint32_t, size_t, uint64_t, c_float, int32_t, uint8_t};
use self::shared_library::dynamic_library::DynamicLibrary;
use std::path::{Path};
use std::ffi::CString;
use dvk::*;
use dvk_khr_surface::*;

VK_DEFINE_NON_DISPATCHABLE_HANDLE!(VkSwapchainKHR);

pub const VK_KHR_SWAPCHAIN_SPEC_VERSION: uint32_t = 68;
pub const VK_KHR_SWAPCHAIN_EXTENSION_NAME: *const c_char = b"VK_KHR_swapchain\0" as *const u8 as *const c_char;

#[repr(i32)]
#[derive(Eq)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum VkKhrSwapchainResult {
    VK_SUCCESS = 0,
    VK_NOT_READY = 1,
    VK_TIMEOUT = 2,
    // VK_EVENT_SET = 3,
    // VK_EVENT_RESET = 4,
    VK_INCOMPLETE = 5,
    VK_ERROR_OUT_OF_HOST_MEMORY = -1,
    VK_ERROR_OUT_OF_DEVICE_MEMORY = -2,
    // VK_ERROR_INITIALIZATION_FAILED = -3,
    VK_ERROR_DEVICE_LOST = -4,
    // VK_ERROR_MEMORY_MAP_FAILED = -5,
    // VK_ERROR_LAYER_NOT_PRESENT = -6,
    // VK_ERROR_EXTENSION_NOT_PRESENT = -7,
    // VK_ERROR_FEATURE_NOT_PRESENT = -8,
    // VK_ERROR_INCOMPATIBLE_DRIVER = -9,
    // VK_ERROR_TOO_MANY_OBJECTS = -10,
    // VK_ERROR_FORMAT_NOT_SUPPORTED = -11,
    VK_ERROR_SURFACE_LOST_KHR = -1000000000,
    VK_ERROR_NATIVE_WINDOW_IN_USE_KHR = -1000000001,
    VK_SUBOPTIMAL_KHR = 1000001003,
    VK_ERROR_OUT_OF_DATE_KHR = -1000001004,
    VK_ERROR_INCOMPATIBLE_DISPLAY_KHR = -1000003001,
    //VK_ERROR_VALIDATION_FAILED_EXT = -1000011001,
    //VK_ERROR_INVALID_SHADER_NV = -1000012000
}

#[repr(u32)]
#[derive(Eq)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum VkKhrSwapchainStructureType {
    VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR = 1000001000,
    VK_STRUCTURE_TYPE_PRESENT_INFO_KHR = 1000001001
}

#[repr(u32)]
#[derive(Eq)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum VkKhrSwapchainImageLayout {
    VK_IMAGE_LAYOUT_PRESENT_SRC_KHR = 1000001002
}

impl From<VkKhrSwapchainImageLayout> for VkImageLayout {
    fn from(imageLayout:VkKhrSwapchainImageLayout) -> Self {
        unsafe {
            std::mem::transmute(imageLayout)
        }
    }
}

pub type VkSwapchainCreateFlagsKHR = VkFlags;

#[repr(C)]
pub struct VkSwapchainCreateInfoKHR {
    pub sType: VkKhrSwapchainStructureType,
    pub pNext: *const c_void,
    pub flags: VkSwapchainCreateFlagsKHR,
    pub surface: VkSurfaceKHR,
    pub minImageCount: uint32_t,
    pub imageFormat: VkFormat,
    pub imageColorSpace: VkColorSpaceKHR,
    pub imageExtent: VkExtent2D,
    pub imageArrayLayers: uint32_t,
    pub imageUsage: VkImageUsageFlags,
    pub imageSharingMode: VkSharingMode,
    pub queueFamilyIndexCount: uint32_t,
    pub pQueueFamilyIndices: *const uint32_t,
    pub preTransform: VkSurfaceTransformFlagBitsKHR,
    pub compositeAlpha: VkCompositeAlphaFlagBitsKHR,
    pub presentMode: VkPresentModeKHR,
    pub clipped: VkBool32,
    pub oldSwapchain: VkSwapchainKHR
}

#[repr(C)]
pub struct VkPresentInfoKHR {
    pub sType: VkKhrSwapchainStructureType,
    pub pNext: *const c_void,
    pub waitSemaphoreCount: uint32_t,
    pub pWaitSemaphores: *const VkSemaphore,
    pub swapchainCount: uint32_t,
    pub pSwapchains: *const VkSwapchainKHR,
    pub pImageIndices: *const uint32_t,
    pub pResults: *mut VkKhrSwapchainResult
}

pub type vkCreateSwapchainKHRFn = unsafe extern "stdcall" fn(device: VkDevice, 
                                                             pCreateInfo: *const VkSwapchainCreateInfoKHR,
                                                             pAllocator: *const VkAllocationCallbacks,
                                                             pSwapchain: *mut VkSwapchainKHR) -> VkKhrSwapchainResult;

pub type vkDestroySwapchainKHRFn = unsafe extern "stdcall" fn(device: VkDevice,
                                                              swapchain: VkSwapchainKHR,
                                                              pAllocator: *const VkAllocationCallbacks);

pub type vkGetSwapchainImagesKHRFn = unsafe extern "stdcall" fn(device: VkDevice,
                                                                swapchain: VkSwapchainKHR,
                                                                pSwapchainImageCount: *mut uint32_t,
                                                                pSwapchainImages: *mut VkImage) -> VkKhrSwapchainResult;

pub type vkAcquireNextImageKHRFn = unsafe extern "stdcall" fn(device: VkDevice,
                                                              swapchain: VkSwapchainKHR,
                                                              timeout: uint64_t,
                                                              semaphore: VkSemaphore,
                                                              fence: VkFence,
                                                              pImageIndex: *mut uint32_t) -> VkKhrSwapchainResult;

pub type vkQueuePresentKHRFn = unsafe extern "stdcall" fn(queue: VkQueue,
                                                          pPresentInfo: *const VkPresentInfoKHR) -> VkKhrSwapchainResult;

#[cfg(windows)]
static VULKAN_LIBRARY: &'static str = "vulkan-1.dll";

#[cfg(unix)]
static VULKAN_LIBRARY: &'static str = "libvulkan-1.so";

#[derive(Default)]
pub struct VulkanKhrSwapchain {
   library: Option<DynamicLibrary>,
   vkGetInstanceProcAddr: Option<vkGetInstanceProcAddrFn>,
   vkCreateSwapchainKHR: Option<vkCreateSwapchainKHRFn>,
   vkDestroySwapchainKHR: Option<vkDestroySwapchainKHRFn>,
   vkGetSwapchainImagesKHR: Option<vkGetSwapchainImagesKHRFn>,
   vkAcquireNextImageKHR: Option<vkAcquireNextImageKHRFn>,
   vkQueuePresentKHR: Option<vkQueuePresentKHRFn>
}

impl VulkanKhrSwapchain {
    pub fn new() -> Result<VulkanKhrSwapchain, String> {
        let mut vulkan_khr_swapchain: VulkanKhrSwapchain = Default::default();
        let library_path = Path::new(VULKAN_LIBRARY);
        vulkan_khr_swapchain.library = match DynamicLibrary::open(Some(library_path)) {
            Err(error) => return Err(format!("Failed to load {}: {}",VULKAN_LIBRARY,error)),
            Ok(library) => Some(library),
        };
        unsafe {
            vulkan_khr_swapchain.vkGetInstanceProcAddr = Some(std::mem::transmute(try!(vulkan_khr_swapchain.library.as_ref().unwrap().symbol::<u8>("vkGetInstanceProcAddr"))));
        }
        Ok(vulkan_khr_swapchain)
    }

    unsafe fn load_command(&self, instance: VkInstance, name: &str) -> Result<vkVoidFunctionFn, String> {
        let fn_ptr = (self.vkGetInstanceProcAddr.as_ref().unwrap())(instance, CString::new(name).unwrap().as_ptr());
        if fn_ptr != std::ptr::null() {
            Ok(fn_ptr)
        } else {
            Err(format!("Failed to load {}",name))
        }
    }

    pub fn load(&mut self, instance: VkInstance) -> Result<(), String> {
        unsafe {
            self.vkCreateSwapchainKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkCreateSwapchainKHR"))));
            self.vkDestroySwapchainKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkDestroySwapchainKHR"))));
            self.vkGetSwapchainImagesKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkGetSwapchainImagesKHR"))));
            self.vkAcquireNextImageKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkAcquireNextImageKHR"))));
            self.vkQueuePresentKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkQueuePresentKHR"))));
        }
        Ok(())
    }

    pub unsafe fn vkCreateSwapchainKHR(&self,
                                         device: VkDevice, 
                                         pCreateInfo: *const VkSwapchainCreateInfoKHR,
                                         pAllocator: *const VkAllocationCallbacks,
                                         pSwapchain: *mut VkSwapchainKHR) -> VkKhrSwapchainResult {
        (self.vkCreateSwapchainKHR.as_ref().unwrap())(device, pCreateInfo, pAllocator, pSwapchain)
    }

    pub unsafe fn vkDestroySwapchainKHR(&self,
                                         device: VkDevice,
                                         swapchain: VkSwapchainKHR,
                                         pAllocator: *const VkAllocationCallbacks) {
        (self.vkDestroySwapchainKHR.as_ref().unwrap())(device,swapchain,pAllocator)
    }
    pub unsafe fn vkGetSwapchainImagesKHR(&self,
                                         device: VkDevice,
                                         swapchain: VkSwapchainKHR,
                                         pSwapchainImageCount: *mut uint32_t,
                                         pSwapchainImages: *mut VkImage) -> VkKhrSwapchainResult {
        (self.vkGetSwapchainImagesKHR.as_ref().unwrap())(device,swapchain,pSwapchainImageCount,pSwapchainImages)
    }

    pub unsafe fn vkAcquireNextImageKHR(&self,
                                         device: VkDevice,
                                         swapchain: VkSwapchainKHR,
                                         timeout: uint64_t,
                                         semaphore: VkSemaphore,
                                         fence: VkFence,
                                         pImageIndex: *mut uint32_t) -> VkKhrSwapchainResult {
        (self.vkAcquireNextImageKHR.as_ref().unwrap())(device,swapchain,timeout,semaphore,fence,pImageIndex)
    }

    pub unsafe fn vkQueuePresentKHR(&self,
                                    queue: VkQueue,
                                    pPresentInfo: *const VkPresentInfoKHR) -> VkKhrSwapchainResult {
        (self.vkQueuePresentKHR.as_ref().unwrap())(queue,pPresentInfo)
    }
}